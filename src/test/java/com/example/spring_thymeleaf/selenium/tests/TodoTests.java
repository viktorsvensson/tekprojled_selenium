package com.example.spring_thymeleaf.selenium.tests;

import com.example.spring_thymeleaf.entities.Todo;
import com.example.spring_thymeleaf.repo.TodoRepo;
import com.example.spring_thymeleaf.selenium.pages.TodoFormPage;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TodoTests extends BaseTest{
    
    @Autowired
    private TodoRepo todoRepo;

    @Test
    @Order(1)
    public void displayExistingTodo() {
        String testTitle = "Test-Title";
        String testDescription = "Test-Description";
        todoRepo.save(new Todo(testTitle, testDescription));

        todoHomePage.refreshPage();

        assertTodoWithTitleAndDescription(testTitle, testDescription);
    }


    @Order(2)
    @Test
    public void addNewTodoTest() throws InterruptedException {
        String testTitle = "Test-Title";
        String testDescription = "Test-Description";

        Thread.sleep(3000);
        TodoFormPage todoFormPage = todoHomePage.clickAddTodoButton();
        Thread.sleep(2000);
        todoFormPage.typeIntoTitleField(testTitle);
        Thread.sleep(2000);
        todoFormPage.typeIntoDescField(testDescription);
        Thread.sleep(2000);
        todoFormPage.clickSaveButton();

        assertTodoWithTitleAndDescription(testTitle, testDescription);
    }

    private void assertTodoWithTitleAndDescription(String testTitle, String testDescription) {
        assertEquals(testTitle, driver.findElement(By.xpath("//h3[text()='" + testTitle + "']")).getText());
        assertEquals(testDescription, driver.findElement(By.xpath("//p[text()='" + testDescription + "']")).getText());
    }

}
