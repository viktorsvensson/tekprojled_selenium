package com.example.spring_thymeleaf.selenium.tests;

import com.example.spring_thymeleaf.repo.TodoRepo;

import com.example.spring_thymeleaf.selenium.pages.TodoHomePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class BaseTest {

    protected WebDriver driver;
    public TodoHomePage todoHomePage;
    private final String baseUrl = "http://localhost:8080/todos";

    @Autowired
    private TodoRepo todoRepo;


    @BeforeAll
    public static void init(){
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    public void setup(){
        todoRepo.deleteAll();
        driver = new ChromeDriver();
        // only for moving it on multimonitor
        driver.manage().window().setPosition(new Point(2000, 0));
        driver.manage().window().maximize();
        driver.get(baseUrl);
        todoHomePage = new TodoHomePage();
        todoHomePage.setDriver(driver);
    }


}
