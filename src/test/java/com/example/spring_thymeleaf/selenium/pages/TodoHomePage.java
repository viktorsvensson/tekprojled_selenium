package com.example.spring_thymeleaf.selenium.pages;

import org.openqa.selenium.By;

public class TodoHomePage extends BasePage {

    private final By addTodoLinkButton = By.linkText("Add Todo");

    public TodoFormPage clickAddTodoButton(){
        click(addTodoLinkButton);
        return new TodoFormPage();
    }

}
