package com.example.spring_thymeleaf.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class BasePage {

    public static WebDriver driver;

    public void setDriver(WebDriver driver){
        BasePage.driver = driver;
    }

    public void refreshPage(){
        driver.navigate().refresh();
    }

    protected WebElement find(By locator){
        return driver.findElement(locator);
    }


    protected void click(By locator){
        find(locator).click();
    }

    protected void sendKeys(By locator, String input){
        find(locator).sendKeys(input);
    }

    protected String getText(By locator){
        return find(locator).getText();
    }
}
