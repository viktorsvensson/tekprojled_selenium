package com.example.spring_thymeleaf.selenium.pages;

import org.openqa.selenium.By;

public class TodoFormPage extends BasePage {

    private final By titleField = By.id("title-field");
    private final By descriptionField = By.id("description-field");
    private final By submitButton = By.id("submit-button");

    public void typeIntoTitleField(String title){
        sendKeys(titleField, title);
    }

    public void typeIntoDescField(String desc){
        sendKeys(descriptionField, desc);
    }

    public void clickSaveButton(){
        click(submitButton);
    }

}
