package com.example.spring_thymeleaf.controller;

import com.example.spring_thymeleaf.dto.CreateTodoDTO;
import com.example.spring_thymeleaf.entities.Todo;
import com.example.spring_thymeleaf.service.TodoService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/todos")
public class TodoController {

    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public String findAll(
            @RequestParam(required = false, defaultValue = "", name = "contains") String contains,
            Model model
    ){
        List<Todo> todos = todoService.findAll(contains);
        model.addAttribute("todoList", todos);
        return "todo";
    }

    @PostMapping
    public String addTodo(@ModelAttribute CreateTodoDTO createTodoDTO){
        todoService.addTodo(createTodoDTO.title(), createTodoDTO.description());
        return "redirect:/todos";
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id){
        todoService.deleteById(id);
    }

}
